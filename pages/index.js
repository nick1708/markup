import React from "react";
import { withLayout } from "@moxy/next-layout";
import PrimaryLayout from "../components/Layout/Layout";
import Carousel from "../components/Home/Carousel";
import Information from "../components/Home/Information";
import Slide from "../components/Home/Slide";
import Image from "next/image";

const HomePage = () => (
  <div>
    <Carousel useDots={true}>
      <Slide
        title="Lorem ipsum dolor sit amet lorem ipsum dolor sit amet dolor"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
				massa nisi, eleifend lacinia augue sit amet, ullamcorper
				placerat erat consectetur adipiscing elit lorem."
      />
      <Slide
        title="Lorem ipsum dolor sit amet lorem ipsum dolor sit amet dolor"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
				massa nisi, eleifend lacinia augue sit amet, ullamcorper
				placerat erat consectetur adipiscing elit lorem."
      />
      <Slide
        title="Lorem ipsum dolor sit amet lorem ipsum dolor sit amet dolor"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
				massa nisi, eleifend lacinia augue sit amet, ullamcorper
				placerat erat consectetur adipiscing elit lorem."
      />
      <Slide
        title="Lorem ipsum dolor sit amet lorem ipsum dolor sit amet dolor"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
				massa nisi, eleifend lacinia augue sit amet, ullamcorper
				placerat erat consectetur adipiscing elit lorem."
      />
    </Carousel>
    <Information
      title="Lorem ipsum"
      text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Quisque posuere libero aliquam libero volutpat, imperdiet tempor sapien sagittis. 
            Aenean magna magna, dignissim a tellus iaculis, venenatis eleifend metus. Maecenas sodales lacinia nulla, 
            et aliquet risus feugiat ut. Nullam congue maximus lorem. Etiam rhoncus quam vitae tortor viverra, non facilisis magna porta. 
            Nulla semper maximus dolor, ut luctus ante pharetra quis. Donec scelerisque mi id neque vehicula, non maximus lorem vestibulum. 
            Aliquam vel vulputate metus. Quisque in porttitor velit, rutrum rutrum turpis. Nulla facilisi. Duis accumsan vitae arcu sed ultricies. 
            Donec magna elit, efficitur non ligula sit amet, gravida efficitur est."
    />
    <div className="Categories">
      <div className="row">
        <div className="col-12 col-md-4 Categories-without-padding mobile">
          <div className="Categories-box packs">
            <span className="Categories-name">Packs</span>
            <button className="Categories-btn">Ver más</button>
          </div>
        </div>
        <div className="col-12 col-md-8 Categories-without-padding">
          <div className="Categories-box preserves">
            <span className="Categories-name white">Conservas</span>
            <button className="Categories-btn">Ver más</button>
          </div>
        </div>
        <div className="col-12 Categories-without-padding">
          <div className="Categories-box vinegars">
            <span className="Categories-name white">Conservas</span>
            <button className="Categories-btn">Ver más</button>
          </div>
        </div>
      </div>
    </div>
    <div className="FeaturedProducts">
      <h3 className="FeaturedProducts-title">Productos destacados</h3>
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-6 col-lg-3">
            <div className="FeaturedProducts-wrapper">
              <img src="/images/home/featuredProducts-1.jpg" alt="" />
              <span className="FeaturedProducts-name">Nombre del producto</span>
              <div className="FeaturedProducts-prices">
                <span className="FeaturedProducts-price prev">30,oo€</span>
                <span className="FeaturedProducts-price">25,oo€</span>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 col-lg-3">
            <div className="FeaturedProducts-wrapper">
              <img src="/images/home/featuredProducts-1.jpg" alt="" />
              <span className="FeaturedProducts-name">Nombre del producto</span>
              <div className="FeaturedProducts-prices">
                <span className="FeaturedProducts-price prev">30,oo€</span>
                <span className="FeaturedProducts-price">25,oo€</span>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 col-lg-3">
            <div className="FeaturedProducts-wrapper">
              <img src="/images/home/featuredProducts-1.jpg" alt="" />
              <span className="FeaturedProducts-name">Nombre del producto</span>
              <div className="FeaturedProducts-prices">
                <span className="FeaturedProducts-price prev">30,oo€</span>
                <span className="FeaturedProducts-price">25,oo€</span>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 col-lg-3">
            <div className="FeaturedProducts-wrapper">
              <img src="/images/home/featuredProducts-1.jpg" alt="" />
              <span className="FeaturedProducts-name">Nombre del producto</span>
              <div className="FeaturedProducts-prices">
                <span className="FeaturedProducts-price prev">30,oo€</span>
                <span className="FeaturedProducts-price">25,oo€</span>
              </div>
            </div>
          </div>
        </div>
        <div className="w-100 text-center">
          <button className="FeaturedProducts-btn">Tornar a la botiga</button>
        </div>
      </div>
    </div>
    <div className="HorizontalMosaic">
      <div className="HorizontalMosaic-wrapper">
        <img
          className="HorizontalMosaic-image"
          src="/images/home/horizontal-mosaic-1.jpg"
          alt=""
        />
        <div className="HorizontalMosaic-info">
          <span className="HorizontalMosaic-text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin et
            eleifend tortor, eget iaculis ipsum. Suspendisse id sodales nisl.
            Nullam aliquam lorem sed orci bibendum faucibus non nec lorem.
          </span>
          <button className="HorizontalMosaic-btn">Lorem ipsum</button>
        </div>
      </div>

      <div className="HorizontalMosaic-wrapper reverse">
        <img
          className="HorizontalMosaic-image"
          src="/images/home/horizontal-mosaic-1.jpg"
          alt=""
        />
        <div className="HorizontalMosaic-info clear">
          <span className="HorizontalMosaic-text brown">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin et
            eleifend tortor, eget iaculis ipsum. Suspendisse id sodales nisl.
            Nullam aliquam lorem sed orci bibendum faucibus non nec lorem.
          </span>
          <button className="HorizontalMosaic-btn">Lorem ipsum</button>
        </div>
      </div>
    </div>
    <div className="FeaturedArticles">
      <h3 className="FeaturedArticles-title">Artículos destacados</h3>
      <div className="FeaturedArticles-container">
        <div className="FeaturedArticles-box">
          <img
            src="/images/home/featured-article-1.jpg"
            alt=""
            className="FeaturedArticles-image"
          />
          <div className="FeaturedArticles-info">
            <span>12/09/20</span> | Nombre Apellido
            <span className="FeaturedArticles-category">Categoría1</span>
          </div>
          <div className="FeaturedArticles-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </div>
        </div>
        <div className="FeaturedArticles-box">
          <img
            src="/images/home/featured-article-2.jpg"
            alt=""
            className="FeaturedArticles-image"
          />
          <div className="FeaturedArticles-info">
            <span>12/09/20</span> | Nombre Apellido
            <span className="FeaturedArticles-category">Categoría1</span>
          </div>
          <div className="FeaturedArticles-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </div>
        </div>
        <div className="FeaturedArticles-box">
          <img
            src="/images/home/featured-article-3.jpg"
            alt=""
            className="FeaturedArticles-image"
          />
          <div className="FeaturedArticles-info">
            <span>12/09/20</span> | Nombre Apellido
            <span className="FeaturedArticles-category">Categoría1</span>
          </div>
          <div className="FeaturedArticles-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </div>
        </div>
      </div>
      <div className="w-100 text-center">
        <button className="FeaturedArticles-btn">Volver al blog</button>
      </div>
      <div className="FeaturedArticles-mosaic">
        <div className="FeaturedArticles-article">
          <div className="FeaturedArticles-backdrop">
            <span>@vallesmercat</span>
          </div>
          <img src="/images/home/article-mosaic-1.jpg" alt="" />
        </div>
        <div className="FeaturedArticles-articles">
          <img
            className="FeaturedArticles-mosaic-item"
            src="/images/home/article-mosaic-2.jpg"
            alt=""
          />
          <img
            className="FeaturedArticles-mosaic-item"
            src="/images/home/article-mosaic-3.jpg"
            alt=""
          />
          <img
            className="FeaturedArticles-mosaic-item"
            src="/images/home/article-mosaic-4.jpg"
            alt=""
          />
          <img
            className="FeaturedArticles-mosaic-item"
            src="/images/home/article-mosaic-5.jpg"
            alt=""
          />
          <img
            className="FeaturedArticles-mosaic-item"
            src="/images/home/article-mosaic-6.jpg"
            alt=""
          />
          <img
            className="FeaturedArticles-mosaic-item"
            src="/images/home/article-mosaic-7.jpg"
            alt=""
          />
        </div>
      </div>
    </div>

    <div className="ContactUs">
      <h3 className="ContactUs-title">Contacta con nosotros</h3>
      <div className="ContactUs-info">
        <span className="ContactUs-text">Email</span>
        <span className="ContactUs-descrciption">hola@logotipo.com</span>
        <span className="ContactUs-text">Teléfono</span>
        <span className="ContactUs-descrciption">+34 626502425</span>
        <span className="ContactUs-text">Dirección</span>
        <span className="ContactUs-descrciption">
          Nombre de la calle <br />
          Barcelona, 08022
        </span>
        <div className="ContactUs-social">
          <i className="i-twitter"></i>
          <i className="i-facebook"></i>
          <i className="i-instagram"></i>
        </div>
      </div>
    </div>
    <div className="Subscription">
      <div className="Subscription-container">
        <div className="Subscription-wrapper">
          <div className="Subscription-description">
            <span className="Subscription-title">
              ¿Quieres recibir nuestras novedades?
            </span>
            <div className="Subscription-text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin et
              eleifend tortor, eget iaculis ipsum. Suspendisse id sodales nisl.
              Nullam aliquam lorem sed orci bibendum faucibus non nec lorem
              ipsum dolor sit amet
            </div>
          </div>
          <div className="Subscription-form">
            <form>
              <div className="form-group mb-1">
                <input className="Input" type="text" placeholder="Nombre" />
              </div>
              <div className="form-group mb-1">
                <input className="Input" type="email" placeholder="Email" />
              </div>
              <button className="Subscription-btn">Suscribirse</button>
              <div className="form-group">
                <label for="subscription">
                  <input
                    className="Subscription-checkbox"
                    type="checkbox"
                    id="subscription"
                  />
                  Acepto recibir emails comerciales de acuerdo con los{" "}
                  <span className="Subscription-link">
                    términos y condiciones
                  </span>
                  y la{" "}
                  <span className="Subscription-link">
                    política de privacidad.
                  </span>
                </label>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div className="OurClients">
      <h3 className="OurClients-title">Nuestros clientes /marcas</h3>
      <div className="OurClients-container d-none d-md-block">
        <Carousel useArrows={true}>
          <div className="OurClients-items">
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
          </div>
          <div className="OurClients-items">
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
          </div>
          <div className="OurClients-items">
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
          </div>
        </Carousel>
      </div>
      <div className="OurClients-container d-block d-md-none">
        <Carousel useArrows={true}>
          <div className="OurClients-items">
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
          </div>
          <div className="OurClients-items">
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
          </div>
          <div className="OurClients-items">
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
          </div>
          <div className="OurClients-items">
            <div className="OurClients-item">
              <img src="/images/home/image-icon-mini.jpg" alt="" />
            </div>
          </div>
        </Carousel>
      </div>
    </div>
  </div>
);

export default withLayout(<PrimaryLayout />)(HomePage);
