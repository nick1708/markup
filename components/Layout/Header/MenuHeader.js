import React, { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { Dropdown, DropdownToggle, DropdownMenu } from "reactstrap";

const HeaderMenu = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen((prevState) => !prevState);

  const [openMenu, setOpenMenu] = useState(false);

  const toggleMenu = () => setOpenMenu(!openMenu);
  return (
    <div className="Header-menu">
      <div className="Header-wrapper">
        <Link href="/">
          <Image
            src="/images/home/logo.png"
            alt="Logo"
            width="121"
            height="49"
          />
        </Link>
        <nav className="Header-nav">
          <ul className="Header-items">
            <li className="Header-item">Tienda</li>
            <li className="Header-item">Actualidad</li>
            <li className="Header-item">Nosotros</li>
            <li className="Header-item">Contacto</li>
          </ul>
        </nav>
        <div className="Header-actions">
          <Dropdown isOpen={dropdownOpen} toggle={toggle}>
            <DropdownToggle className="Header-language">CAT</DropdownToggle>
            <DropdownMenu>
              <span className="d-block">CAT</span>
              <span className="d-block">EN</span>
              <span className="d-block">ES</span>
            </DropdownMenu>
          </Dropdown>
          <Dropdown>
            <DropdownToggle>
              <i className="i-search" />
            </DropdownToggle>
          </Dropdown>
          <Dropdown>
            <DropdownToggle>
              <i className="i-acount" />
            </DropdownToggle>
          </Dropdown>
          <Dropdown>
            <DropdownToggle>
              <i className="i-basket" />
            </DropdownToggle>
          </Dropdown>
        </div>
        <div className="Header-mobile-icons d-block d-lg-none">
          <button onClick={toggleMenu} className="Header-btn">
            <i className="i-hamburger"></i>
          </button>
        </div>
      </div>
      <div
        className={openMenu ? "Header-mobileMenu active" : "Header-mobileMenu"}
      >
        <div className="Header-mobileMenu-actions">
          <div className="Header-mobileMenu-wrapper">
            <button className="Header-mobileMenu-btn">
              <i className="i-search" />
            </button>
            <button className="Header-mobileMenu-btn">
              <i className="i-acount" />
            </button>
            <button className="Header-mobileMenu-btn">
              <i className="i-basket" />
            </button>
          </div>
          <button onClick={toggleMenu} className="Header-mobileMenu-close">
            x
          </button>
        </div>
        <div className="Header-mobileMenu-items">
          <div className="Header-mobileMenu-link">
            <Link href="/">Tienda</Link>
          </div>
          <div className="Header-mobileMenu-link">
            <Link href="/">Actualidad</Link>
          </div>
          <div className="Header-mobileMenu-link">
            <Link href="/">Nosotros</Link>
          </div>
          <div className="Header-mobileMenu-link">
            <Link href="/">Contacto</Link>
          </div>
          <div className="Header-mobileMenu-link">
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle className="Header-mobileMenu-language">
                CAT
              </DropdownToggle>
              <DropdownMenu>
                <span className="d-block">CAT</span>
                <span className="d-block">EN</span>
                <span className="d-block">ES</span>
              </DropdownMenu>
            </Dropdown>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderMenu;
