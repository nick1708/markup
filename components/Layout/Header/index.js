import React from "react";
import TopbarHeader from "./TopbarHeader";
import MenuHeader from "./MenuHeader";

const Header = () => {
	return (
		<div className="Header">
			<TopbarHeader />
			<MenuHeader />
		</div>
	);
};

export default Header;
