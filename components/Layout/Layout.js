import React from "react";
import Head from "next/head";
import Header from "./Header";
import Footer from "./Footer";

export default function Layout(props) {
	return (
		<div className="Layout">
			<Head>
				<title>Home</title>
				<link href="/css/main.css" rel="stylesheet" />
			</Head>
			<Header />
			{props.children}
			<Footer />
		</div>
	);
}
