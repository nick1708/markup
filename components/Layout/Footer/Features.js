import React from "react";

const FeaturesFooter = () => {
	return (
		<div className="Features">
			<div className="Features-container">
				<div className="Features-item">
					<i className="i-quality"></i>
					<span>Garantia de qualitat</span>
				</div>
				<div className="Features-item">
					<i className="i-safePurchase"></i>
					<span>Compra segura</span>
				</div>
				<div className="Features-item">
					<i className="i-transport"></i>
					<span>Envíos gratis a 24/48h</span>
				</div>
				<div className="Features-item">
					<i className="i-return"></i>
					<span>Devolucions en 15 dies</span>
				</div>
			</div>
		</div>
	);
};

export default FeaturesFooter;
