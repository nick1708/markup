import React, { Fragment } from "react";
import Features from "./Features";
import Content from "./Content";

const Footer = () => {
	return (
		<Fragment>
			<Features />
			<Content />
		</Fragment>
	);
};

export default Footer;
