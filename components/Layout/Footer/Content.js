import React, { Fragment } from "react";

const ContentFooter = (props) => {
	return (
		<Fragment>
			<div className="Footer">
				<div className="Footer-container">
					<div className="Footer-row">
						<div className="Footer-items">
							<span className="Footer-title">Sobre Sentits</span>
							<span className="Footer-option">
								La Distribuidora
							</span>
							<span className="Footer-option">La botiga</span>
							<span className="Footer-option">L’equip</span>
							<span className="Footer-option">
								Treballa amb nosaltres
							</span>
							<span className="Footer-option">Exportació</span>
							<span className="Footer-option">Cellers</span>
						</div>
						<div className="Footer-items">
							<span className="Footer-title">Productes</span>
							<span className="Footer-option">Vinos</span>
							<span className="Footer-option">Espumosos</span>
							<span className="Footer-option">Vermuts</span>
							<span className="Footer-option">Cerveses</span>
							<span className="Footer-option">
								Altres elaboracions
							</span>
							<span className="Footer-option">Formatges</span>
							<span className="Footer-option">Olis</span>
						</div>
						<div className="Footer-items">
							<span className="Footer-title">
								Atenció al client
							</span>
							<span className="Footer-option">Avís legal</span>
							<span className="Footer-option">
								Política de privacitat
							</span>
							<span className="Footer-option">
								Política de cookies
							</span>
							<span className="Footer-option">
								Condicions generals
							</span>
							<span className="Footer-option">
								Condicions d’enviament
							</span>
							<span className="Footer-option">
								Gastos de transport
							</span>
							<span className="Footer-option">
								Condicions de devolució
							</span>
						</div>
						<div className="Footer-items">
							<span className="Footer-title">
								Formas de pagament
							</span>
							<img src="/images/home/paymethod.jpg" alt="" />
						</div>
						<div className="Footer-items">
							<span className="Footer-title">Segueix-nos a</span>
							<div className="Footer-social">
								<i className="i-footer-twitter"></i>
								<i className="i-footer-facebook"></i>
								<i className="i-footer-instagram"></i>
								<i className="i-footer-linkedin"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="Footer-beonshop">
				<span>Work done by</span>
				<img
					className="Footer-logo"
					src="/images/icons/beonshop-logo.svg"
					alt=""
				/>
			</div>
		</Fragment>
	);
};

export default ContentFooter;
