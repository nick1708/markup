import React from "react";
import Carousel from "@brainhubeu/react-carousel";
import Slide from "../Slide";
import "@brainhubeu/react-carousel/lib/style.css";

const CarouselHome = (props) => {
	const { useDots, useArrows } = props;
	return (
		<div className="Carousel">
			<Carousel dots={useDots} arrows={useArrows}>
				{props.children}
			</Carousel>
		</div>
	);
};

export default CarouselHome;
