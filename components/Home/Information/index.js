import React from "react";

const Information = (props) => {
	return (
		<section className="Information">
			<h3 className="Information-title">{props.title}</h3>
			<span className="Information-text">{props.text}</span>
			<button className="Information-btn">Más información</button>
		</section>
	);
};

export default Information;
