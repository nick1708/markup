import React from "react";

const Slide = (props) => {
	return (
		<div className="Slide">
			<h2 className="Slide-title">{props.title}</h2>
			<span className="Slide-text">{props.text}</span>
		</div>
	);
};

export default Slide;
